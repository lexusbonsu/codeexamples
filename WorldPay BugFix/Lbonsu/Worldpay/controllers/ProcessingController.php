<?php

require_once 'Phoenix/Worldpay/controllers/ProcessingController.php';

class Lbonsu_Worldpay_ProcessingController extends Phoenix_Worldpay_ProcessingController {

    /**
     * when customer selects Worldpay payment method
     */
    public function redirectAction()
    {
        try {

            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }

            /*
             * Fix for Worldpay bug
             * Initially, customers who DIDN'T complete their orders and returned to the site had their basket/session cleared
             * To fix this we'll make sure the order has been paid for before clearing the session
             */
            if ($order->getState() != Mage_Sales_Model_Order::STATE_PENDING_PAYMENT && $order->getBaseTotalDue() > 0) {
                $order->setState(
                    Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                    $this->_getPendingPaymentStatus(),
                    Mage::helper('worldpay')->__('Customer was redirected to Worldpay.')
                )->save();
            }
            if ($session->getQuoteId() && $session->getLastSuccessQuoteId() && $order->getBaseTotalDue() == 0) {
                $session->setWorldpayQuoteId($session->getQuoteId());
                $session->setWorldpaySuccessQuoteId($session->getLastSuccessQuoteId());
                $session->setWorldpayRealOrderId($session->getLastRealOrderId());
                $session->getQuote()->setIsActive(false)->save();
                $session->clear();
            }
            $this->loadLayout();
            $this->renderLayout();
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            $this->_debug('Worldpay error: ' . $e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }
}