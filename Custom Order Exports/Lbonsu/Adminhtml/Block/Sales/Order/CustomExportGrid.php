<?php

class Lbonsu_Adminhtml_Block_Sales_Order_CustomExportGrid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Retrieve collection class
     *
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'sales/order_grid_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $select = $collection->getSelect();
        $resource = Mage::getSingleton('core/resource');
        $select->joinLeft(
            array('shipping' => $resource->getTableName('sales/order_address')),
            //The shipping address is always used as priority (where applicable)
            'IFNULL(main_table.shipping_address_id, main_table.billing_address_id) = shipping.entity_id'
        );

        $select->where('main_table.updated_at >= CURDATE() - INTERVAL 1 DAY')
               ->where('main_table.status = "complete"')
               ->where('main_table.payment_type IN (?)', array('payment_type_1','payment_type_2','payment_type_3'))
               ->where('main_table.can_send_confirmation_email = "1"');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order number'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
            'filter_index' => 'increment_id',
        ));

        $this->addColumn('original_order_id', array(
            'header'=> Mage::helper('sales')->__('Original Order number'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'original_order_id',
            'filter_index' => 'original_order_id',
        ));

        $this->addColumn('crate_volume', array(
            'header' => Mage::helper('sales')->__('Crate Volume'),
            'index' => 'number_of_boxes',
        ));

        $this->addColumn('name_1', array(
            'header' => Mage::helper('sales')->__('Name 1'),
            'type' => 'text',
            'index' => 'name_1',
        ));

        $this->addColumn('name_2', array(
            'header' => Mage::helper('sales')->__('Name 2'),
            'type' => 'text',
            'index' => 'name_2',
        ));

        $this->addColumn('street_1', array(
            'header' => Mage::helper('sales')->__('Street 1'),
            'renderer' => 'Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street1',
        ));

        $this->addColumn('street_2', array(
            'header' => Mage::helper('sales')->__('Street 2'),
            'renderer' => 'Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street2',
        ));

        $this->addColumn('city', array(
            'header' => Mage::helper('sales')->__('City'),
            'index' => 'city',
        ));

        $this->addColumn('postcode', array(
            'header' => Mage::helper('sales')->__('Postcode'),
            'index' => 'postcode',
        ));

        $this->addColumn('shipping_telephone', array(
            'header' => Mage::helper('sales')->__('Contact phone number'),
            'index' => 'shipping_telephone',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('sales')->__('Email'),
            'index' => 'customer_email',
        ));

        $this->addColumn('delivery_date_1', array(
            'header' => Mage::helper('sales')->__('Delivery Date 1'),
            'index' => 'delivery_date_1',
            'type' => 'date',
        ));

        $this->addColumn('delivery_date_2', array(
            'header' => Mage::helper('sales')->__('Delivery Date 2'),
            'index' => 'delivery_date_2',
            'type' => 'date',
        ));

        $this->addColumn('return_date_1', array(
            'header' => Mage::helper('sales')->__('Return Date 1'),
            'index' => 'return_date_1',
            'type' => 'date',
        ));

        $this->addColumn('return_date_2', array(
            'header' => Mage::helper('sales')->__('Return Date 2'),
            'index' => 'return_date_2',
            'type' => 'date',
        ));

        $this->addColumn('payment_type', array(
            'header' => Mage::helper('sales')->__('Payment Type'),
            'index' => 'payment_type',
            'type' => 'text',
        ));

        $collection = $this->prepareProductColumns();

        return parent::_prepareColumns();
    }

    protected function prepareProductColumns()
    {
        //Removed
        //Returned relevant products
    }

    public function createExportOrdersCsv(){
        $dailyExport = true;
        $this->_prepareColumns();
        $grid = $this->_prepareCollection($dailyExport);
        $collection = $grid->getCollection();
        $size = $collection->getSize();

        if ($size > 0) {
            $io = new Varien_Io_File();

            $path = Mage::getBaseDir('var') . DS . 'export' . DS;
            $date = new DateTime();;
            $file = $path . DS . 'orders_export'. $date->format('Y-m-d') .'.csv';

            $io->setAllowCreateFolders(true);
            $io->open(array('path' => $path));
            $io->streamOpen($file, 'w+');
            $io->streamLock(true);
            $io->streamWriteCsv($this->_getExportHeaders());

            $this->_exportIterateCollection('_exportCsvItem', array($io));

            if ($this->getCountTotals()) {
                $io->streamWriteCsv(
                    Mage::helper("core")->getEscapedCSVData($this->_getExportTotals())
                );
            }

            $io->streamUnlock();
            $io->streamClose();
            return $file;
        }
        return;
    }
}
