<?php

class Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street2
    extends Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street {

    public function render(Varien_Object $row) {

        $street = $row->getAddress();

        return $this->getStreet(2, $street);
    }
    
}
?>