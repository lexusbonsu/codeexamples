<?php

class Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected function getStreet($line=0, $street)
    {
        if (-1 === $line) {
            return $street;
        } else {
            $arr = is_array($street) ? $street : explode("\n", $street);
            if (0 === $line || $line === null) {
                return $arr;
            } elseif (isset($arr[$line-1])) {
                return $arr[$line-1];
            } else {
                return '';
            }
        }
    }
}
?>