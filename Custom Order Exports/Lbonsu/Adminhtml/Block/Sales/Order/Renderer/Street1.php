<?php

class Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street1
    extends Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Street {

    public function render(Varien_Object $row) {

        $street = $row->getAddress();

        return $this->getStreet(1, $street);
    }
}
?>