<?php

class Lbonsu_Adminhtml_Block_Sales_Order_Renderer_AccountCode
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData($this->getColumn()->getIndex());
        $value = str_pad($value, 8, "0", STR_PAD_LEFT);
        return $value;
    }
}
?>