<?php

class Lbonsu_Adminhtml_Block_Sales_Order_Renderer_Product
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $productName = $this->getColumn()->getId();
        $orderId = $row->getEntityId();
        $order = Mage::getModel('sales/order')->load($orderId);
        $items = $order->getAllVisibleItems();
        foreach($items as $item){
            if($item->getName() == $productName){
                return $item->getQtyOrdered();
            }
        }
        return false;
    }
}
?>