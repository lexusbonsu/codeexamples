<?php

class Lbonsu_Warehouse_Model_Api_Inventory_Order extends Lbonsu_Warehouse_Model_Api_Abstract
{

    public function __construct()
    {
        parent::__construct();
        $this->wsdl = Mage::getStoreConfig("lbonsuwarehouse/api_settings/api_".$this->apimode."_order_url");
        $this->connect();
    }

    public function sendOrder($order)
    {
        $order = Mage::helper('lbonsu_warehouse')->formatOrder($order);

        try{
            $response = $this->warehouseCall('Import',$order,'SalesOrders');
            return $response;
        } catch (Exception $e) {
            Mage::log($e,Zend_Log::DEBUG,'WarehouseApiError.log',true);
            return;
        }
    }
}