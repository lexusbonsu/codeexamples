<?php

class Lbonsu_Warehouse_Model_Api_Inventory_Stock extends Lbonsu_Warehouse_Model_Api_Abstract
{


    public function __construct()
    {
        parent::__construct();
        $this->wsdl = Mage::getStoreConfig("lbonsuwarehouse/api_settings/api_".$this->apimode."_stock_url");
        $this->connect();
    }

    /**
     * Update product stock in Warehouses ERP System
     */
    public function syncProducts()
    {
        try{
            $stockResponse = $this->warehouseCall('Stocks','Full','Detaillevel');
            //Log error message
            if(!is_array($stockResponse))
            {
                Mage::log($e,Zend_Log::DEBUG,'WarehouseApiResponse.log',true);
            }
            Mage::helper('lbonsu_warehouse')->setStock($stockResponse,$this->warehouse);
        } catch (Exception $e)
        {
            Mage::log($e,Zend_Log::DEBUG,'WarehouseApiError.log',true);
            return;
        }
    }

}