<?php

class Lbonsu_Warehouse_Model_Api_Abstract
{
    protected   $apimode;
    protected   $warehouse;
    private     $username;
    private     $password;
    private     $customerId;
    private     $messageId;
    protected   $wsdl;
    private     $client = null;

    public function __construct()
    {
        $this->apimode      = Mage::getStoreConfig('lbonsuwarehouse/api_settings/api_mode');
        $this->warehouse    = Mage::getStoreConfig('lbonsuwarehouse/api_settings/warehouse');
        $this->username     = Mage::getStoreConfig('lbonsuwarehouse/api_settings/username');
        $this->password     = Mage::getStoreConfig('lbonsuwarehouse/api_settings/password');
        $this->messageId    = (string)rand('1','100000');
        $this->customerId   = Mage::getStoreConfig('lbonsuwarehouse/api_settings/customer_id');
    }

    /**
     * This function implements a WS-Security digest authentication for PHP.
     *
     * @access private
     * @param string $username
     * @param string $password
     * @return SoapHeader
     */
    private function getWSSecurityHeader($username,$password)
    {
        // Initializing namespaces
        $ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';

        // Creating WSS identification header using SimpleXML
        $root = new SimpleXMLElement('<root/>');
        $security = $root->addChild('wsse:Security', null, $ns_wsse);
        $usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
        $usernameToken->addChild('wsse:Username', $username, $ns_wsse);
        $usernameToken->addChild('wsse:Password', Mage::helper('core')->decrypt($password), $ns_wsse)->addAttribute('Type', $password_type);

        // Recovering XML value from that object
        $root->registerXPathNamespace('wsse', $ns_wsse);
        $full = $root->xpath('/root/wsse:Security');
        $auth = $full[0]->asXML();

        return new SoapHeader($ns_wsse, 'Security', new SoapVar($auth, XSD_ANYXML), true);
    }

    /**
     * Attempt connection to Warehouse API
     */
    protected function connect()
    {
        try{
            $this->client = new SoapClient($this->wsdl,array('trace'=>1));
        } catch (Exception $e)
        {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            return;
        }
        $this->client->__setSoapHeaders($this->getWSSecurityHeader($this->username, $this->password));
    }

    /**
     * Make API Call
     * @param string $function
     * @param array $options
     * @param string $attribute
     * @return array
     */
    protected function warehouseCall($function,$options,$attribute)
    {
        $params = array(
            'MessageId'=>$this->messageId,
            'CustomerId'=>$this->customerId,
            $attribute=>$options
        );

        try {
            $response = $this->client->__soapCall($function,array($params));
            Mage::log($response,Zend_Log::DEBUG,'WarehouseApiResponse.log',true);
            switch ($function)
            {
                case 'Import':
                    return array(
                        'code'          => $response->ImportResult->ResponseItems->ResponseItem->Code,
                        'description'   => $response->ImportResult->ResponseItems->ResponseItem->Description,
                        'describing'    => $response->ImportResult->ResponseItems->ResponseItem->Describing
                    );
                case  'Stocks':
                    return $response->StocksResult->ItemStocks->ItemStock;
            }
        } catch (Exception $e)
        {
            Mage::log($e->getMessage(),Zend_Log::DEBUG,'WarehouseApiError.log',true);
            return;
        }
    }
}