<?php

class Lbonsu_Warehouse_Model_Observer extends Mage_Core_Helper_Abstract
{
    public function uploadOrder($observer)
    {
        $order      = $observer->getInvoice()->getOrder();
        Mage::log('Observing order: '. $order->getId(), null, 'warehouse.log', true);
        $session    = Mage::getSingleton('core/session');
        /*if( Mage::getStoreConfig('lbonsuwarehouse/api_settings/api_order_sync') != 'enabled'
          ||  $order->getAssignationWarehouse() != Mage::getStoreConfig('lbonsuwarehouse/api_settings/assignation_warehouse')
        ) {*/
        if ($order->getStore()->getCode() != 'eu') {
            Mage::log('Order did not match assignation warehouse: '. $order->getId(), null, 'warehouse.log', true);
            Mage::log('Assignation warehouse: '. print_r($order->getAssignationWarehouse(), true), null, 'warehouse.log', true);
            return;
        }

        $warehouseApi = Mage::getModel('lbonsu_warehouse/api_inventory_order')->sendOrder($order);

        if($warehouseApi['code'] == '200') {
            $session->addSuccess('The order has been successfully uploaded to Warehouse\'s ERP');
            $order->addStatusHistoryComment('Order Uploaded to Warehouse\'s ERP')->save();
        } else {
            //Catch other error(s) with API
            Mage::log($warehouseApi,Zend_Log::DEBUG,'WarehouseApiError.log',true);
            $session->addError('Error uploading order to Warehouse ERP');
        }
    }

    public function syncProducts()
    {
        if( Mage::getStoreConfig('lbonsuwarehouse/api_settings/api_stock_sync') != 'enabled') {
            return;
        }
        Mage::getModel('lbonsu_warehouse/api_inventory_stock')->syncProducts();
    }

}
