<?php

class Lbonsu_Warehouse_Model_System_Config_Source_Sync
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'enabled', 'label' => Mage::helper('lbonsu_warehouse')->__('Enabled')),
            array('value' => 'disabled', 'label' => Mage::helper('lbonsu_warehouse')->__('Disabled')),
        );
    }

    public function toArray()
    {
        return $this->toOptionArray();
    }
}

