<?php

class Lbonsu_Warehouse_Model_System_Config_Source_Mode
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'staging', 'label' => Mage::helper('lbonsu_warehouse')->__('Staging')),
            array('value' => 'live', 'label' => Mage::helper('lbonsu_warehouse')->__('Live')),
        );
    }

    public function toArray()
    {
        return $this->toOptionArray();
    }
}

