<?php

class Lbonsu_Warehouse_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Convert Magento order to Warehouse API compatible order
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function formatOrder($order)
    {
        $warehouseOrder = array();
        $products = $this->formatProducts($order->getAllVisibleItems());
        $warehouseOrder[] = array(
                'ExternalDocumentNumber'    =>  $order->getId(),
                'DocumentCreationDate'      =>  date("Y-m-d",strtotime($order->getCreatedAt())),
                'DocumentCreationUser'      =>  $order->getCustomerName(),
                'Type'                      =>  'B2C',
                'Template'                  =>  'DEFAULT',
                'Shipping'                  =>  array('ModeOfTransportation'=>'STANDARD','Address'=>$this->formatAddress($order->getShippingAddress())),
                'Positions'                 =>  $products
        );

        return $warehouseOrder;
    }

    /**
     * Convert Magento products to Warehouse API compatible product
     *
     * @param Mage_Sales_Model_Order_Item $products as $product
     * @return array
     */
    public function formatProducts($products)
    {
        $warehouseProducts = array();
        $lineNumber = 1;
        foreach ($products as $key => $product) {
            $warehouseProducts['Position'][] = array(
                'Quantity' => intval($product->getQtyOrdered()),
                'LineNumber' => $lineNumber,
                'ArticleNumber' => $product->getSku()
            );
            $lineNumber++;
        }
        return $warehouseProducts;
    }

    /**
     * Convert Magento products to Warehouse API compatible product
     *
     * @param array $products
     * @return array
     */
    public function setProductIds($products)
    {
        foreach ($products as $product)
        {
            $id = Mage::getModel('catalog/product')->getResource()->getIdBySku($product->ArticleNumber);
            if ($id) {
                $product->id = $id;
            }
        }
        return $products;
    }

    /**
     * Set new stock qty with data retrieved from Warehouse API
     *
     * @param array $products
     * @param sting $warehouseId
     * @return array
     */
    public function setStock($products,$warehouseId)
    {
        $warehouse = Mage::getModel('advancedinventory/api');
        //Get product ids
        $products = $this->setProductIds($products);
        foreach ($products as $product)
        {
            $this->setStockData($product->id,$warehouseId,array('quantity_in_stock'=>$product->CurrentAvailable));
        }
    }

    public function setStockData($product_id, $warehouse_id,
            $data = array("manage_stock" => 1, "quantity_in_stock" => 0, "backorder_allowed" => 0, "use_config_setting_for_backorders" => 1)) 
    {

        try {
            $wyomindApi = Mage::getModel('advancedinventory/api');
            $stock = Mage::getModel('advancedinventory/stock')->getStockByProductIdAndPlaceId($product_id, $warehouse_id);


            $origin_qty = $stock->getQuantityInStock();
            $data["id"] = $stock->getId();
            $data["place_id"] = $warehouse_id;
            $data["product_id"] = $product_id;
            $data["localstock_id"] = Mage::getModel('advancedinventory/item')->loadByProductId($product_id)->getId();

            if ($stock->getQuantity_in_stock() != $data['quantity_in_stock'] || $stock->getUse_config_setting_for_backorders() != $data['use_config_setting_for_backorders'] || $stock->getManageStock() != $data['manage_stock'] || $stock->getBackorder_allowed() != $data['backorder_allowed']) {
                $wyomindApi->insertLogRow("API", "Set Data", "P#$product_id,W#$warehouse_id", "Qty : " . (int) $stock->getQuantity_in_stock() . " -> " . $data['quantity_in_stock'] . ", Manage stock = " . (int) $data['manage_stock'] . ", Backorder = " . $data['backorder_allowed'] . ", Use config setting = " . $data['use_config_setting_for_backorders'], "WAREHOUSE UPDATE");
                $stock->setData($data)->save();
            }

            $inventory = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_id);
            $new_qty = $inventory->getQty() - $origin_qty + $data['quantity_in_stock'];


            if ((int) $inventory->getQty() != $new_qty) {
                $wyomindApi->insertLogRow("API", "Set Data", "P#$product_id", "Qty : " . (int) $inventory->getQty() . " -> " . $new_qty, "WAREHOUSE UPDATE");
            }
            $inventory->setQty($new_qty);
            $inventory->save();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function formatAddress($address)
    {
        $warehouseAddress = array(
            'CustomerID' =>     $address->getCustomerId(),
            'Name' =>           $address->getLastname(),
            'Firstname' =>      $address->getFirstname(),
            'Street' =>         $address->getStreetFull(),
            'PostCode' =>       $address->getPostcode(),
            'City' =>           $address->getCity(),
            'State' =>          $address->getRegion(),
            'CountryCode' =>    $address->getCountryId(),
            'Phone' =>          $address->getTelephone(),
            'Email' =>          $address->getEmail()
        );
        return $warehouseAddress;
    }
}
